# Ryfall [ryfall.herokuapp.com](https://ryfall.herokuapp.com/)

A not-quite-feature-complete [Spyfall](http://international.hobbyworld.ru/spyfall) clone, built as a learning project.

### Disclaimer

Ryfall is not endorsed by Spyfall designer, Alexandr Ushan, or publisher, [HobbyWorld](http://international.hobbyworld.ru/).

### Going Forward

TBD. Probably gonna clean it up and add a few more features.

### Technologies
- [Django](https://www.djangoproject.com/)
- [Channels](https://channels.readthedocs.io/en/latest/)
- [React](https://reactjs.org/)