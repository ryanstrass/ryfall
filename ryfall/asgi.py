import os
import django
from channels.routing import get_default_application

# run with: daphne -p $PORT ryfall.asgi:application
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ryfall.settings")
django.setup()
application = get_default_application()
