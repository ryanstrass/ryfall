from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.Lobby)
admin.site.register(models.Connection)
admin.site.register(models.Location)
admin.site.register(models.Role)
