import json
import random
from datetime import timedelta

from django.utils.timezone import now
from django.db.models import Q

from channels.generic.websocket import JsonWebsocketConsumer
from asgiref.sync import async_to_sync

from . import models


class LobbyConsumer(JsonWebsocketConsumer):
    lobby = None
    connection = None
    access_code = None

    @property
    def group_name(self):
        if self.access_code:
            return f"lobby-{self.access_code}"
        else:
            raise Exception('No access code!')

    def connect(self):
        self.access_code = self.scope["url_route"]["kwargs"]["lobby"]
        gamertag = self.scope["url_route"]["kwargs"]["gamertag"]
        self.accept()

        # find lobby
        try:
            self.lobby = models.Lobby.objects.get(access_code=self.access_code)
        except:
            print('lobby not found')
            self.close(code=4040)
            return

        # create connection (allow duplicate names)
        try:
            self.connection = models.Connection.objects.create(
                lobby = self.lobby,
                username=gamertag
            )
        except Exception as e:
            print('could not create connection')
            self.close(code=4041)
            return

        # add consumer to group
        async_to_sync(self.channel_layer.group_add)(
            self.group_name, self.channel_name
        )

        # signal group to update
        async_to_sync(self.channel_layer.group_send)(
            self.group_name, { "type": "update.players" }
        )

        # assign role if game in progress
        if self.lobby.game:
            models.Connection.objects.assign_role(self.lobby, self.connection)
            self.connection.refresh_from_db()

            self.update_lobby({})


    def update_players(self, event):
        self.send(json.dumps({
            "type": "update_players",
            "player": {
                "name": self.connection.username
            },
            "players": self.other_players()
        }))

    def receive_json(self, content):
        cmd = content.get('command', None)
        if cmd == "close_socket":
            self.close()
        if cmd == "change_name":
            self.change_name(content["name"])
        if cmd == "start_game":
            self.start_game()

    def change_name(self, new_name):
        self.connection.username = new_name
        self.connection.save()

        async_to_sync(self.channel_layer.group_send)(
            self.group_name,
            {
                "type": "update.players"
            }
        )


    def start_game(self):
        game = models.Game.objects.create_game(lobby=self.lobby)

        async_to_sync(self.channel_layer.group_send)(
            self.group_name,
            {
                "type": "game.started"
            }
        )


    def update_lobby(self, message):
        # update lobby & connection
        self.lobby.refresh_from_db()
        self.connection.refresh_from_db()

        a,b = self.lobby.game.split_possible()

        self.send(json.dumps({
            "type": "lobby",
            "game": {
                "end": str(self.lobby.game.end if self.lobby.game else None),
                "locations": {
                    "a": a,
                    "b": b
                }
            },
            "player": {
                "name": self.connection.username,
                "role": self.connection.role,
                "location": self.connection.location
            },
            "players": self.other_players()
        }))

    def other_players(self):
        all_players = models.Connection.objects.filter(
            Q(lobby=self.lobby) & ~Q(pk=self.connection.pk)
        )

        return [p.username for p in all_players]

    def game_started(self, message):
        self.update_lobby(message)

    def spy_disconnect(self, message):
        self.send(json.dumps({
            "type": "spy_disconnect"
        }))

    def disconnect(self, close_code):
        if self.connection:
            if self.connection.role == "spy":
                async_to_sync(self.channel_layer.group_send)(
                    f"lobby-{self.access_code}",
                    {
                        "type": "spy.disconnect"
                    }
                )

            self.connection.delete()

        async_to_sync(self.channel_layer.group_send)(
            self.group_name,
            {
                "type": "update.players"
            }
        )

        if self.lobby:
            async_to_sync(self.channel_layer.group_discard)(
                self.group_name,
                self.channel_name
            )

    def close_socket(self):
        self.close()
