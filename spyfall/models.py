from django.db import models
from django.utils import timezone
from datetime import timedelta, datetime
import random
import uuid

TIME_BETWEEN_NEW_GAMES = 5 # seconds
GAME_DURATION = 480 # seconds
NUM_GAME_LOCATIONS = 10

def short_access_code(length=6):
    chars = 'abcdefghijkmnpqrstuvwxyz23456789'
    code = ''
    for i in range(0, length):
        code += random.choice(chars)
    return code

class LocationManager(models.Manager):

    def random(self, n):
        locations = [obj for obj in self.get_queryset().all()]

        # could consider raising exception instead
        if n > len(locations):
            n = len(locations)

        random.shuffle(locations)

        return locations[:n]


class Location(models.Model):
    name = models.CharField(max_length=40)

    objects = LocationManager()

    def __str__(self):
        return self.name


class Role(models.Model):
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    name = models.CharField(max_length=40)

    def __str__(self):
        return str(self.location) + " - " + self.name

class GameManager(models.Manager):
    def create_game(self, lobby):
        if not lobby:
            raise Exception('no lobby')

        if lobby.game and lobby.game.start_time + timedelta(seconds=TIME_BETWEEN_NEW_GAMES) > timezone.now():
            return

        locations = Location.objects.random(NUM_GAME_LOCATIONS)
        game = self.create(
            location=random.choice(locations),
            start_time=timezone.now()
        )
        game.possible.set(locations)

        lobby.game = game
        lobby.save()
        Connection.objects.assign_roles(lobby=lobby)

        return game

class Game(models.Model):
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    start_time = models.DateTimeField(default=timezone.now)
    possible = models.ManyToManyField(Location, related_name='games_where_pos')

    objects = GameManager()

    @property
    def end(self):
        return self.start_time + timedelta(seconds=GAME_DURATION)

    def split_possible(self, as_str=True):
        if as_str:
            all = [p.name for p in self.possible.all()]
        else:
            all = self.possible.all()

        mid = len(all)//2
        return all[:mid], all[mid:]

class Lobby(models.Model):
    access_code = models.CharField(unique=True, max_length=6, default=short_access_code)
    game = models.ForeignKey(Game, on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return self.access_code


class ConnectionManager(models.Manager):

    def assign_roles(self, lobby):
        connections = self.get_queryset().filter(lobby=lobby)
        roles = Role.objects.filter(location=lobby.game.location)

        for connection in connections:
            connection.location = lobby.game.location.name
            connection.role = random.choice(roles).name
            connection.save()

        # randomly select spy
        spy = random.choice(connections)
        spy.location = '???'
        spy.role = 'spy'
        spy.save()


    def assign_role(self, lobby, connection):
        roles = Role.objects.filter(location=lobby.game.location)
        connection.location = lobby.game.location.name
        connection.role = random.choice(roles).name
        connection.save()

class Connection(models.Model):
    #id = models.UUIDField(default=uuid.uuid4)
    lobby = models.ForeignKey(Lobby, on_delete=models.CASCADE)
    username = models.CharField(max_length=100)

    role = models.CharField(max_length=40, blank=True, null=True)
    location = models.CharField(max_length=40, blank=True, null=True)

    objects = ConnectionManager()

    def __str__(self):
        return str(self.lobby) + " - " + self.username
