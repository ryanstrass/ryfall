from rest_framework import serializers
from . import models

class LobbySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Lobby
        fields = '__all__'
        read_only_fields = ('access_code','game')
