from django.shortcuts import render
from rest_framework import viewsets

from . import models, serializers

class LobbyViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.LobbySerializer
    queryset = models.Lobby.objects.all()
