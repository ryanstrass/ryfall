from django.shortcuts import render

# renders basic page containing react element
def index(request):
    return render(request, 'frontend/index.html')
