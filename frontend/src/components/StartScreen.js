import React, {Component} from "react";
import Link from "react-router-dom";
import {Route, Redirect} from "react-router";
import { Grid, Message, Header, Button, Icon, Input, Loader } from 'semantic-ui-react';
import Cookies from "js-cookie";
import NameInput from "./NameInput";

const MAX_NAME_LENGTH = 100;

export default class StartScreen extends Component {

  state = {
    gamertag: '',
    lobby: '',
    newGame: false,
    joinGame: false,
    errors: {
      gamertag: '',
      lobby: ''
    }
  }

  onJoinGame = e => {
    this.setState({joinGame: true});
    this.setState({newGame: false});
  }

  onNewGame = e => {
    this.setState({newGame: true});
    this.setState({joinGame: false});
  }

  onCancel = e => {
    this.clearState();
  }

  clearState = () => {
    this.setState({
      gamertag: '',
      lobby: '',
      newGame: false,
      joinGame: false,
      loading: false,
      errors: {
        gamertag: '',
        lobby: ''
      }
    });
  }

  hasValidGamertag() {
    var _gamertag = encodeURIComponent(this.state.gamertag);
    return (1 <= _gamertag.length && _gamertag.length <= MAX_NAME_LENGTH);
  }

  hasValidLobbyCode() {
    return (this.state.lobby.length === 6);
  }

  onStart = e => {
    this.updateErrors();

    if (this.state.joinGame && this.hasValidGamertag() && this.hasValidLobbyCode()) {
      this.enterLobby();
    }

    if (this.state.newGame && this.hasValidGamertag()) {
      this.setState({loading: true}, this.createNewLobby());
    }
  }

  createNewLobby() {
    const game = fetch(`/gameapi/lobbies/`, {
      method: "post",
      credentials: "same-origin",
      headers: {
        "X-CSRFToken": Cookies.get("csrftoken"),
        "Content-Type": "application/json"
      }
    })
    .then(res => {
      if (res.status < 500) {
        // handle all server errors (update later!)
        return res.json().then(data => {
          return {status: res.status, data}
        })
      } else {
        console.log('server error');
        throw res;
      }
    })
    .then(res => {
      if (res.status === 201) {
        // lobby created. redirect using access_code
        this.setState({ lobby: res.data.access_code }, () => {
          this.enterLobby()
        });
        return res.data;
      } else if (res.status >= 400 && res.status < 500) {
        throw res.data;
      }
    })
  }

  enterLobby() {
    this.props.history.push(`/${this.state.lobby}`);
  }

  onChangeName = name => {
    // clear errors
    if (this.state.errors.gamertag) {
      this.setState({errors: {...this.state.errors, gamertag: ''}})
    }

    this.setState({gamertag: name}, () => {
      sessionStorage.setItem('gamertag', this.state.gamertag);
    });
  }

  onChangeLobby = e => {
    // clear errors
    if (this.state.errors.lobby) {
      this.setState({errors: {...this.state.errors, lobby: ''}});
    }

    // update
    this.setState({lobby: e.target.value});
  }

  updateErrors = () => {
    var lobbyErr = this.state.lobby.length !== 6 ? 'invalid lobby code' : '';
    var gamertagErr = this.state.gamertag.length < 1 ? 'cannot be blank' : '';
    gamertagErr = this.state.gamertag.length > 30 ? 'too long' : gamertagErr;

    this.setState({
      errors: {...this.state.errors, lobby: lobbyErr, gamertag: gamertagErr}
    });
  }

  renderDefault = () => (
    <div>
      <Header as='h3' block>
        <Grid columns={2}>
          <Grid.Column width={13}><Grid.Row>Welcome to Ryfall</Grid.Row></Grid.Column>
          <Grid.Column width={1}><Grid.Row><Icon name='spy' /></Grid.Row></Grid.Column>
        </Grid>
      </Header>
      <Grid columns="equal" style={{marginTop: "4rem"}}>
        <Grid.Column width={8}>
          <Grid.Row>
            <Button fluid onClick={this.onNewGame}>
              New Game
            </Button>
          </Grid.Row>
        </Grid.Column>
        <Grid.Column width={8}>
          <Grid.Row>
            <Button fluid onClick={this.onJoinGame}>
              Join Game
            </Button>
          </Grid.Row>
        </Grid.Column>
      </Grid>
    </div>
  )

  renderNewGame() {
    return (
      <div>
        <Header as='h3' style={{marginBottom: '3em' }} block>
          {
            "Start a new game"
          }
        </Header>

        {/* New game fields */}
        <NameInput name={this.state.gamertag} maxLength={MAX_NAME_LENGTH} onChange={this.onChangeName} />

        {/* New game action buttons */}
        <div style={{margin: 'auto', marginTop: '.5em'}}>
          <Button icon labelPosition='left' onClick={this.onCancel}>
            Cancel
            <Icon name='cancel' />
          </Button>
          {this.state.loading
            ?
            <Button disabled loading>Start</Button>
            :
            <Button icon labelPosition='right' onClick={this.onStart}>
              Start
              <Icon name='right arrow' />
            </Button>
          }
        </div>
      </div>
    );
  }

  renderJoinGame() {
    return (
      <div>
        <Header as='h3' style={{marginBottom: '3em' }} block>{"Join a game"}</Header>

        {/* Join game fields */}
        <Input
          error={this.state.errors.lobby ? true : false}
          fluid
          style={{marginBottom: '.5em'}}
          id='lobby'
          placeholder='Lobby code'
          value={this.state.lobby}
          onChange={this.onChangeLobby}/>

        <NameInput name={this.state.gamertag} maxLength={MAX_NAME_LENGTH} onChange={this.onChangeName} />

        {/* Join game action buttons */}
        <div style={{margin: 'auto', marginTop: '.5em'}}>
          <Button icon labelPosition='left' onClick={this.onCancel}>
            Cancel
            <Icon name='cancel' />
          </Button>
          <Button icon labelPosition='right' onClick={this.onStart}>
            Join
            <Icon name='right arrow' />
          </Button>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div style={{maxWidth: 400, margin: "auto"}}>
        {/* Default state */}
        {(!this.state.newGame && !this.state.joinGame) &&
          this.renderDefault()
        }

        {/* New game state */}
        {this.state.newGame &&
          this.renderNewGame()
        }

        {/* Join game state */}
        {this.state.joinGame &&
          this.renderJoinGame()
        }
      </div>
    );
  }
}
