import React, { Component } from "react";
import { Input, Message } from "semantic-ui-react";

export default class NameInput extends Component {
  constructor (props) {
    super(props)

    this.state = {
      error: ''
    }
  }

  componentDidMount() {
    this.validate(this.props.name);
  }

  validate(name) {
    var encodedName = encodeURIComponent(name);
    if (encodedName.length > this.props.maxLength) {
      this.setState({error: 'Name is too long.'})
    } else {
      this.setState({error: ''});
    }
  }

  handleChange = e => {
    this.validate(e.target.value);
    this.props.onChange(e.target.value);
  }

  render() {
    return (
      <div>
        <Input
          error={this.state.error.length >= 1}
          fluid
          id='gamertag'
          placeholder='Display name'
          value={this.props.name}
          onChange={this.handleChange}
          />
        {this.state.error &&
          <Message warning size='mini'>{this.state.error}</Message>
        }
      </div>
    );

  }
}
