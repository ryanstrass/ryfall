import React, {Component} from "react";
import { Message, Header, List, Input, Button, Icon, Grid, Divider } from 'semantic-ui-react';

import NameInput from "./NameInput";
import ReconnectingWebSocket from "./ReconnectingWebSocket";

const LOCALHOST = '127.0.0.1:8000';
const MAX_NAME_LENGTH = 100;
const GAME_DURATION = 8;


const dateFromString = s => {
  var m = s.toString().match(/(\d+)-(\d+)-(\d+)\s+(\d+):(\d+):(\d+)\.(\d+)/);
  return new Date(+m[1], +m[2] - 1, +m[3], +m[4], +m[5], +m[6] * 100);
}

export default class Game extends Component {

  state = {
    lobbyNotFound: false,

    // player
    gamertag: '',
    loggedIn: false,

    //game
    loading: false,
    spyDC: false,
    inProgress: false,
    interval: null,

    game: {
      end: null,
      locations: {
        a: [],
        b: []
      }
    },
    player: {
      name: '',
      role: '',
      location: ''
    },
    players: [],

    // websocket
    socket: null,

    errors: {
      gamertag: '',
      lobby: ''
    }
  }

  getHost() {
    const hostname = window.location.hostname;
    return (LOCALHOST.includes(hostname) ? LOCALHOST : hostname);
  }

  constructWSPath() {
    const scheme = window.location.protocol == "https:" ? "wss" : "ws";
    const host = this.getHost();
    const access_code = this.props.match.params.code.toLowerCase();
    const gamertag = encodeURIComponent(this.state.gamertag);

    return `${scheme}://${host}/ws/${access_code}/${gamertag}`;
  }

  connect() {
    const _socket = new ReconnectingWebSocket(this.constructWSPath());

    _socket.onopen = () => {
      console.log('connected.');
      this.setState({closed: false});
    }

    _socket.onmessage = e => {
      console.log('received message');
      this.handleMessage(e.data);
    }

    _socket.onerror = e => {
      console.log(e);
    }

    _socket.onclose = e => {
      console.log(e);
      if (e.code === 4040) {
        console.log(`lobby ${this.props.match.params.code} does not exist.`)
        _socket.close();
        console.log('closed.');
        this.setState({lobbyNotFound: true});
      }
    }

    this.setState({socket: _socket});
  }

  disconnect() {
    this.state.socket.close();
  }

  updateGamertag() {
    this.state.socket.send(
      JSON.stringify({command: "change_name", name: this.state.gamertag})
    );
  }

  handleMessage(msg) {
    const _msg = JSON.parse(msg);
    console.log(_msg);

    if (_msg.type === "update_players") {
      this.updatePlayers(_msg);
    }
    if (_msg.type === "spy_disconnect") {
      this.setState({spyDC: true});
    }
    if (_msg.type === "lobby") {
      this.setState({
        ...this.state,
        loading: false,
        inProgress: true,
        timeRemaining: '',
        spyDC: false,
        game: {
          ..._msg.game,
          end: new Date(_msg.game.end)
        },
        player: _msg.player,
        players: _msg.players
      });
    }
  }

  updatePlayers(msg) {
    const players = msg.players.map(p => {
      return decodeURIComponent(p);
    })
    this.setState({
      ...this.state,
      player: {
        ...this.state.player,
        name: msg.player.name,
      },
      players: players
    });
  }

  hasValidGamertag() {
    var _gamertag = encodeURIComponent(this.state.gamertag);
    return (1 <= _gamertag.length && _gamertag.length <= MAX_NAME_LENGTH);
  }

  onChangeName = name => {
    // clear errors
    if (this.state.errors.gamertag) {
      this.setState({errors: {...this.state.errors, gamertag: ''}})
    }

    this.setState({gamertag: name}, () => {
      sessionStorage.setItem('gamertag', this.state.gamertag);
    });
  }

  onNewName = e => {
    this.setState({ loggedIn: false });
  }

  onLeave = e => {
    this.setState({loggedIn: false}, () => {
      this.cleanUp();
      this.props.history.push('/');
    });
  }

  cleanUp() {
    sessionStorage.setItem('gamertag', '');
    this.state.socket && this.disconnect();
    this.interval && clearInterval(this.interval);
  }

  onStartGame = e => {
    console.log('starting game');
    this.setState({loading: true}, () => {
      this.state.socket.send(JSON.stringify({ command: 'start_game' }))
    });
  }

  onJoin = e => {
    if (this.hasValidGamertag()) {
      if (this.state.socket) {
        this.updateGamertag();
        this.setState({loggedIn: true});
      } else {
        this.logIn();
      }
    }
  }

  logIn() {
      this.setState({loggedIn: true}, () => {
        if (!this.state.socket) {
          this.connect();
        }
      })
  }

  logOut() {
    this.setState({loggedIn: false}, () => {
      if (this.state.socket) {
        this.disconnect();
      }
    })
  }

  updateTimeRemaining = () => {

    if (!this.state.game.end || this.state.game.end <= Date.now()) {
      return;
    }

    const remaining = new Date(this.state.game.end - Date.now()).toISOString().slice(14, -5);
    if (this.mounted) {
      this.setState({
        timeRemaining: remaining,
      }, () => {
        this.interval = setInterval(this.updateTimeRemaining, 500);
        clearInterval(this.interval);
      });
    }
  }

  componentDidMount() {
    // check local storage for gamertag from StartScreen
    const _gamertag = sessionStorage.getItem('gamertag');

    this.setState({gamertag: _gamertag ? _gamertag : ''}, () => {
      if (this.state.gamertag && !this.state.lobbyNotFound) {
        this.logIn()
      } else {
        this.logOut()
      }
    })

    // timer setup
    this.mounted = true;
    this.interval = setInterval(this.updateTimeRemaining, 500);
  }

  componentWillUnmount() {
    this.mounted = false; // async timer fix
    this.cleanUp();
  }

  renderNameSelect = () => (
    <div>
      <Header as='h3' style={{marginBottom: '3em' }} block> {"Pick a name"} </Header>
      <NameInput
        name={this.state.gamertag}
        maxLength={MAX_NAME_LENGTH}
        onChange={this.onChangeName} />
      <div style={{margin: 'auto', marginTop: '.5em'}}>
        <Button icon
          style={{marginRight: 'auto'}}
          labelPosition='right'
          onClick={this.onJoin}>
          Join
          <Icon name='right arrow' />
        </Button>
      </div>
    </div>
  )

  renderLobby(){
    return (
      <div>
        {(this.state.players && this.state.player.name) &&
          <div style={{marginBottom: '4em'}}>

            {/* List of players */}
            <Header as='h5'>Players</Header>
            <Grid columns={1}>
              <Grid.Column>
                <Grid.Row key={0}>{this.state.player.name} <a style={{cursor: "pointer"}} onClick={this.onNewName}>(edit)</a></Grid.Row>
                  {this.state.players.map((player, idx) => (
                    <Grid.Row key={idx+1}>
                      {player}
                    </Grid.Row>
                  ))}
              </Grid.Column>
            </Grid>

            {/* List of locations */}
            {this.state.game.locations.a.length > 0 &&
              <>
                <Header as='h5'>Possible Locations</Header>
                <Grid columns={2}>
                  <Grid.Column width={8}>
                    {this.state.game.locations.a.map((location, idx) => (
                      <Grid.Row key={`a${idx}`}>{location}</Grid.Row>
                    ))}
                  </Grid.Column>
                  <Grid.Column width={8}>
                    {this.state.game.locations.b.map((location, idx) => (
                      <Grid.Row key={`b${idx}`}>{location}</Grid.Row>
                    ))}
                  </Grid.Column>
                </Grid>
              </>

            }


            {/* Bottom action buttons: Leave, Start */}
            <div style={{marginTop: '15%'}}>
              <Button icon
                labelPosition='left'
                onClick={this.onLeave}>
                Leave Lobby
                <Icon name='left arrow' />
              </Button>
              {
                this.state.loading
                ?
                <Button disabled loading>Start Round</Button>
                :
                <Button onClick={this.onStartGame}> Start Round </Button>
              }
            </div>
          </div>
        }

      </div>
    );
  }

  render() {
    const code = this.props.match.params.code;
    return (
      <div style={{maxWidth: 300, margin: "auto"}}>
        {this.state.lobbyNotFound &&
          <div>
            <Header as='h3'>Lobby not found!</Header>
              <Button icon
                labelPosition='left'
                onClick={this.onLeave}>
                Back
                <Icon name='left arrow' />
              </Button>
          </div>
        }
        {!this.state.lobbyNotFound &&
          <div>
            <Header as="h3">@{code.toLowerCase()}</Header>
            {this.state.spyDC &&
              <Message negative> <p>{"Spy disconnected. Start a new game."}</p> </Message>
            }
            {this.state.inProgress &&
              <Header as="h2" color={(this.state.game.end - Date.now()) < 60000 ? "red" : "black"} style={{margin: "auto"}}>
                {this.state.timeRemaining}
              </Header>
            }
            {this.state.player.role &&
              <div>
                <p>You are <strong>{this.state.player.role}</strong> at <strong>{this.state.player.location}</strong>.</p>
                <br />
              </div>
            }
            { (this.state.loggedIn) ? this.renderLobby() : this.renderNameSelect() }
          </div>
        }
      </div>
    );
  }
}
