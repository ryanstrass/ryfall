import React, { Component } from "react";
import { Container, Header, Divider, List, Label, Grid } from "semantic-ui-react";

export default class Rules extends Component {


  render() {
    return (
      <Container>
        <Header as='h4' inverted>Roles</Header>
        <List>
          <List.Item>One <Label color='teal' size='tiny'>Spy</Label>, many <Label color='purple' size='tiny'>Non-Spies</Label></List.Item>
        </List>
        <Header as='h4' inverted>Objectives</Header>
        <Grid >
          <Grid.Row columns={2} style={{paddingTop: '.5rem', paddingBottom: '.25rem'}}>
            <Grid.Column width={4}><Label color='teal' size='tiny'>Spy</Label></Grid.Column>
            <Grid.Column width={10}>Uncover the location.</Grid.Column>
          </Grid.Row>
          <Grid.Row columns={2} style={{paddingTop: '.25rem', paddingBottom: '.5rem'}}>
            <Grid.Column width={4}><Label color='purple' size='tiny'>Non-Spy</Label></Grid.Column>
            <Grid.Column width={10}>Uncover the imposter.</Grid.Column>
          </Grid.Row>
        </Grid>
        <Header as='h4' inverted>Gameplay</Header>
        <List bulleted>
          <List.Item>
            <p>Spyfall is played in <span style={{color: 'gold'}}>8-minute</span> rounds.</p>
          </List.Item>
          <List.Item>
            Each round, a <Label color='teal' size='mini'>Spy</Label> is randomly selected and the other players are given a random location and role.
          </List.Item>
          <List.Item>
            The random location is the same for all players, but the roles may vary.
          </List.Item>
          <List.Item>
            Once the timer starts, players take turns asking each other <em>subtle</em> questions.
          </List.Item>
          <List.Item>
            <Label color='purple' size='mini'>Non-Spies</Label> try to identify their allies and prove their innocence
            while not giving away clues to the <Label color='teal' size='mini'>Spy</Label>.
          </List.Item>
          <List.Item>
            The <Label color='teal' size='mini'>Spy</Label> asks and answers questions inconspicuously,
            trying to discern the location.
          </List.Item>
        </List>

        <Header as='h4' inverted>Winning the game</Header>
        <List bulleted inverted>
          <List.Item>
            <List.Content>
              <List.Header>The timer runs out.</List.Header>
              <List.Description>
                Once the timer runs out, the game enters the accusation phase.
                No more questions are allowed. Players are free to speculate and
                attempt to convince others of another player's guilt or innocence.
                Players may <em>unanimously</em> agree to accuse another player
                of being a <Label color='teal' size='mini'>Spy</Label>.
              </List.Description>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>A unanimous accusation is made.</List.Header>
              <List.Description>
                On a player's turn during the game (instead of asking a question),
                or during the accusation phase, that player may accuse another player of being a <Label color='teal' size='mini'>Spy</Label>.
                Each player (except the accused) must vote <em>Yea</em> or <em>Nay</em>. If all players vote <em>Yea</em>,
                the accused must reveal if they are a <Label color='teal' size='mini'>Spy</Label>.
                If the accused is the <Label color='teal' size='mini'>Spy</Label>, the
                <Label color='purple' size='mini'>Non-Spies</Label> win.
                If not, the <Label color='teal' size='mini'>Spy</Label> wins.
              </List.Description>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>The <Label color='teal' size='mini'>Spy</Label> makes a guess</List.Header>
              <List.Description>
                At any point in the game, the <Label color='teal' size='mini'>Spy</Label> can interrupt and guess the location.
                Once they do, the game ends. If the <Label color='teal' size='mini'>Spy</Label> guesses the correct location they win.
                If they guess the wrong location, the <Label color='purple' size='mini'>Non-Spies</Label> win.
              </List.Description>
            </List.Content>
          </List.Item>
        </List>
        <Divider />
        <a href="https://www.cryptozoic.com/sites/default/files/icme/u30695/spy_rules_eng_0.pdf" target="_blank" rel="noopener noreferrer">
          Full Spyfall Rules
        </a>
      </Container>
    );
  }
}
