import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

// ui
import { Container } from 'semantic-ui-react';

// components
import StartScreen from "./StartScreen";
import Game from "./Game";
import Footer from "./Footer";

const App = () => (
  <Router>
    <Container style={{ marginTop: '12%'}}>
      <Route exact path="/" component={StartScreen} />
      <Route path="/:code" component={Game} />
      <Footer />
    </Container>
  </Router>
);
const wrapper = document.getElementById("app");
wrapper ? ReactDOM.render(<App />, wrapper) : null;
