import React, {Component} from "react";
import { Link } from "react-router-dom";

import { List, Container, Icon, Label, Divider, Modal, Header } from "semantic-ui-react";

import Rules from "./Rules";

export default class Footer extends Component {

  state = {
    showRules: false
  }

  openRules = e => {
    this.setState({showRules: true});
  }

  closeRules = e => {
    this.setState({showRules: false});
  }

  render() {
    return (
      <Container textAlign='center' style={{marginTop: "15%", marginBottom: "5%"}}>
        <Divider style={{maxWidth: 500, margin: 'auto'}}/>
        <Modal basic size='small' open={this.state.showRules} onClose={this.closeRules} closeIcon>
          <Header>Simplified Rules</Header>
          <Modal.Content>
            <Rules />
          </Modal.Content>
        </Modal>

        <List>
          <List.Item>
            <Label as='a' href="https://www.cryptozoic.com/spyfall" target="_blank" rel="noopener noreferrer">
              Spyfall
            </Label>
            <Label as='a' onClick={this.openRules}>
              <Icon name='game' />
              Rules
            </Label>
            <Label as='a' href="https://gitlab.com/ryanstrass/ryfall" style={{marginTop: 4}} target="_blank" rel="noopener noreferrer">
              <Icon name='gitlab' />
              Sauce
            </Label>
          </List.Item>
        </List>





      </Container>
    );
  }
}
